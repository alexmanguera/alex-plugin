<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   alex-plugin
 * @author    Alex Manguera
 * @link      http://alexmanguera.com
 * @copyright 2016 Alex Manguera
 */
?>
<div class="wrap">
	<table>
    	<tr>
        	<td><img src="<?php echo AP_PLUGIN_URL_PATH; ?>/assets/icon.png" alt="wpms" /></td>
			<td><h2>WPMS - Settings</h2></td>
        </tr>
	</table>
	
	<?php
	if(isset($_SESSION['message']))
	{
		$message = explode("|", $_SESSION['message']);
		if($message[0] == "success")
			echo "<div id='message' class='updated below-h2'><p>".$message[1]."</p></div>";
		else
			echo "<div id='message' class='below-h2 error'><p>".$message[1]."</p></div>";
		ap_deleteTempMessage();
	}
	?>
	
	<form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
		<?php if ( function_exists('wp_nonce_field') ) wp_nonce_field('ap-settings'); ?>
		<input type="hidden" name="save" value="save">
		<input type="hidden" name="action" value="apsubmitsettings">
		
		<div class="">
			<label class="" for="name">Enable (test)</label>
			<div class="">
				<?php 
					if(get_option('alex_plugin_settings') == "true") {
						$checked = 'checked="checked"';
					} else{
						$checked = '';
					}
				?>
				<input type="checkbox" id="test" name="test" <?php echo $checked; ?>>
			</div>
		</div>
		<p class="submit">
			<input type="submit" name="submit" id="submit" class="button button-primary" value="Save Settings">
		</p>
	</form>
</div>