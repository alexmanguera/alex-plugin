<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   alex-plugin
 * @author    Alex Manguera
 * @link      http://alexmanguera.com
 * @copyright 2016 Alex Manguera
 */
?>

<?php
	if($results = ap_dbase_get_specific_experiment( $_GET['expid'] ))
	{
		foreach ($results as $experiment) {
			$experiment_id = $experiment->id;
			$experiment_name = $experiment->name;
			$experiment_description = $experiment->description;
			$experiment_status = $experiment->status;
			$experiment_start_date = $experiment->start_date;
			$experiment_end_date = $experiment->end_date;
			$experiment_goal_type = $experiment->goal_type;
			$experiment_goal = $experiment->goal;
			$experiment_url = $experiment->url;
			$experiment_visits = $experiment->original_visits;
		}
	}	
	$results_avail_experiments = ap_dbase_get_experiments();
	$variations = ap_dbase_get_variations( $experiment->id );
?>
<script type="text/javascript">
window.onload = function () {
	var chart = new CanvasJS.Chart("chartContainer",
	{
		title:{
			text: "<?php echo $experiment_visits; ?> Total Visits "
		},
		legend: {
			maxWidth: 350,
			itemWidth: 120
		},
		data: [
		{
			type: "pie",
			showInLegend: true,
			legendText: "{indexLabel}",
			dataPoints: [
			<?php 
			$i = 0;
			$variation_visits = 0;
			foreach( $variations as $variation_chart ) {
				$i++;
				$variation_visits += $variation_chart->visits;
			?>
				{ y: <?php echo $variation_chart->visits; ?>, indexLabel: "<?php echo $variation_chart->name; ?>" },
		  <?php } ?>
				{ y: <?php echo $experiment_visits-$variation_visits; ?>, indexLabel: "Original" }
			]
		}
		]
	});
	chart.render();
}
</script>

<div class="wrap">
	<table>
    	<tr>
        	<td><img src="<?php echo AP_PLUGIN_URL_PATH; ?>/assets/icon.png" alt="wpms" /></td>
			<td><h2>WPMS - Experiment Details</h2></td>
		</tr>
	</table>
	
	<?php
	if(isset($_SESSION['message']))
	{
		$message = explode("|", $_SESSION['message']);
		if($message[0] == "success")
			echo "<div id='message' class='updated below-h2'><p>".$message[1]."</p></div>";
		else
			echo "<div id='message' class='below-h2 error'><p>".$message[1]."</p></div>";
		ap_deleteTempMessage();
	}
	?>
	
	<div style="float: left;">
		<div id="chartContainer" style="height: 280px; width: 30%;"></div>
	</div>

	<div style="float: left; margin: 0px 0px 50px 530px;">
		<div style="margin-bottom: 20px;">
			<form action="<?php echo admin_url('admin-post.php'); ?>" method="post">
				<?php if ( function_exists('wp_nonce_field') ) wp_nonce_field('ap-settings'); ?>
				<input type="hidden" name="selectexperiment" value="selectexperiment">
				<input type="hidden" name="action" value="apsubmitselectexperiment">
				
				<div class="">
					<label class="" for="name">Choose Experiment:</label>
					<select name="experiments" onChange="this.form.submit();">
						<?php if(!isset($_GET['expid']) || $_GET['expid'] == ""){ ?>
						<option value="">-- select --</option>
						<?php } ?>
						<?php
						foreach ($results_avail_experiments as $avail_experiment) { 
							if($avail_experiment->id == $experiment_id) {
								$selected = ' selected="selected"';
							}else{
								$selected = '';
							}
						?>
						<option value="<?php echo $avail_experiment->id; ?>"<?php echo $selected; ?>>(Exp ID: <?php echo $avail_experiment->id.') '.$avail_experiment->name; ?></option>
						<?php } ?>
					</select>
				</div>
			</form>
		</div>
		
		<div>
			<table>
				<tr>
					<td><strong>Experiment ID:</strong> <?php echo $experiment_id; ?></td>
				</tr>
				<tr>
					<td><strong>Name:</strong> <?php echo ucwords($experiment_name); ?></td>
				</tr>
				<tr>
					<td><strong>Description:</strong> <?php echo $experiment_description; ?></td>
				</tr>
				<tr>
					<td><strong>Status:</strong> <?php echo ucwords($experiment_status); ?></td>
				</tr>
				<tr>
					<td><strong>Start Date:</strong> <?php echo $experiment_start_date; ?></td>
				</tr>
				<tr>
					<td><strong>End Date:</strong> <?php echo $experiment_end_date; ?></td>
				</tr>
				<tr>
					<td><strong>Goal Type:</strong> <?php echo $experiment_goal_type; ?></td>
				</tr>
				<tr>
					<td><strong>Goal:</strong> <?php echo $experiment_goal; ?></td>
				</tr>
				<tr>
					<td><strong>URL:</strong> <a href="<?php echo $experiment_url; ?>" target="_blank"><?php echo $experiment_url; ?></a></td>
				</tr>
				<tr>
					<td><strong>Visits:</strong> <?php echo $experiment_visits; ?></td>
				</tr>
			</table>
		</div>
	</div>
	
	<?php if($variations = ap_dbase_get_variations( $experiment_id )){ ?>
	<div>
	<table class="widefat"  id="variationtable" width="100%">
		<thead>
		    <tr>
		        <th>Action</th>
		        <th>Name</th>
		        <th>Text Variation</th>
		        <th>Visitors</th>       
		        <th>Conversions</th>
		        <th>Conversions Rate</th>
		    </tr>
		</thead>
		<tfoot>
		     <tr>
		        <th>Action</th>
		        <th>Name</th>
		        <th>Text Variation</th>
		        <th>Visitors</th>       
		        <th>Conversions</th>
		        <th>Conversions Rate</th>
		    </tr>
		</tfoot>
		<tbody>
		<?php foreach($variations as $variation) { ?>
			<?php 
			if($_GET['action'] == "edit-variation" && $_GET['varid'] != "" && $variation->id == $_GET['varid'])
			{
			?>
			<form method="post" action="<?php echo admin_url('admin-post.php'); ?>" id="apsubmitupdatedvariation">
			<input type="hidden" name="save" value="save">
			<input type="hidden" name="action" value="apsubmitupdatedvariation">
			<input type="hidden" name="expid" value="<?php echo $experiment_id;?>">
			<input type="hidden" name="varid" value="<?php echo $variation->id;?>">
			<tr>
				<th><a href="this<?php echo $variation->id;?>"></a><input type="submit" name="submit" id="submit" class="submit button button-primary" value="Save"> | <a href="admin.php?page=alex-plugin-experiment-details&expid=<?php echo $experiment_id;?>" class="cancel" onclick="return confirm('Cancel edit of this variation?');">Cancel</a></th>
				<th><input name="variation_name" id="variation_name" type="text" value="<?php echo ucwords($variation->name); ?>" class="regular-text" required></th>
				<th><input name="variation_value" id="variation_value" type="text" value="<?php echo ucwords($variation->value); ?>" class="regular-text" required></th>
				<th><?php echo number_format(ap_dbase_get_specific_total_visits_variation( $variation->id )); ?></th>
				<th><?php //echo number_format($totalVariationConvertion = ab_press_getTotalConvertions($experiment));  ?></th>
				<th><?php //echo ($totalVariationConvertion == 0) ? "0" : ab_press_getConvertionRate($totalVariationConvertion,$totalVariationVisitor);?>%</th>
			</tr>
			</form>
			<?php
			}else{
			?>
			<tr>
				<th><a href="admin.php?page=alex-plugin-experiment-details&action=edit-variation&varid=<?php echo $variation->id; ?>&expid=<?php echo $experiment_id;?>#this<?php echo $variation->id;?>" class="edit">Edit</a> | <a href="admin.php?page=alex-plugin-experiment-details&action=delete-variation&varid=<?php echo $variation->id; ?>&expid=<?php echo $experiment_id;?>" class="delete" onclick="return confirm('Delete Variation: <?php echo $variation->name; ?>?');">Delete</a></th>
				<th><?php echo ucwords($variation->name); ?></a></th>
				<th><?php echo ucwords($variation->value); ?></a></th>
				<th><?php echo number_format(ap_dbase_get_specific_total_visits_variation( $variation->id )); ?></th>
				<th><?php //echo number_format($totalVariationConvertion = ab_press_getTotalConvertions($experiment));  ?></th>
				<th><?php //echo ($totalVariationConvertion == 0) ? "0" : ab_press_getConvertionRate($totalVariationConvertion,$totalVariationVisitor);?>%</th>
			</tr>
			<?php } // end if ?>
		<?php } //end foreach ?>
		</tbody>
	</table>
	</div>
	<?php } // end if ?>
	
</div>