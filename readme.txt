﻿=== Alex Plugin ===
Contributors: Alex Manguera
Tags: A/B Split Testing
Requires at least: 3.4.2
Tested up to: 4.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


== Description ==

Split Testing for Wordpress.