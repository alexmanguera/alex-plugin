<?php
/**
 * Fired when the plugin is uninstalled.
 *
 * @package   alex-plugin
 * @author    Alex Manguera
 * @link      http://alexmanguera.com
 * @copyright 2016 AlexManguera
 */

// If uninstall, not called from WordPress, then exit
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

global $wpdb;

delete_option('alex_plugin');

delete_transient( 'wpms_experiment_status' );

$sql = "DROP TABLE IF EXISTS " . $wpdb->prefix . "'alex_plugin_experiments'";
$wpdb->query($sql);