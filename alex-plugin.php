<?php
/**
 * @package   alex-plugin
 * @author    Alex Manguera
 * @link      http://alexmanguera.com
 * @copyright 2016 AlexManguera
 *
 * @wordpress-plugin
 * Plugin Name: Alex Plugin
 * Plugin URI:  http://alexmanguera.com
 * Description: This plugin is under development.
 * Version:     0.1
 * Author:      Alex Manguera
 * Author URI:  http://alexmanguera.com
 * Text Domain: alex-plugin-locale
 *
 * ------------------------------------------------------------------------
 * Copyright 2016 Alex Plugin ( http://alexmanguera.com )
 *
 **/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// define constant variables here
define('AP_OPTION', 'alex_plugin');
define('AP_OPTION_SETTINGS', 'alex_plugin_settings');
define('AP_PLUGIN_URL_PATH', plugins_url('', __FILE__ ) );
define('AP_PLUGIN_DIR_PATH', plugin_dir_path( __FILE__ ) );
define('AP_COOKIE_EXPIRY', 120 );// 86400 for 1 day
define('AP_EXPERIMENT_LIST_LIMIT', 10 );// limits the number of entries for pagination

require_once( AP_PLUGIN_DIR_PATH . '/includes/functions.php' );

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, 'activate' );
register_deactivation_hook( __FILE__, 'deactivate' );

if(!isset($_SESSION) && is_admin()) {
   session_start();
}

// Add the options page and menu item.
add_action( 'admin_menu', 'ap_establish_menus' );

//delete_transient( 'wpms_experiment_status' );
// Initiate transient / check Experiment status (running, paused or complete)
add_action( 'init', 'ap_check_experiment_status');

add_action( 'init', 'cookie_handler');

// ----------------------------------------------------
// load stylesheet and javascript for both admin and frontend all the time.
/* 
wp_enqueue_style( 'apPluginStylesheet', AP_PLUGIN_URL_PATH . '/css/admin.css' );
wp_enqueue_script( 'apPluginScript', AP_PLUGIN_URL_PATH . '/js/admin.js' );
*/
// ----------------------------------------------------
//-----------------------------------------------------
// load stylesheet and/or javascript only on admin.
/* 
function load_admin_ap_plugin_js_style() {
	wp_enqueue_style( 'apPluginStylesheet', AP_PLUGIN_URL_PATH . '/css/admin.css' );
	wp_enqueue_script( 'apPluginScript', AP_PLUGIN_URL_PATH . '/js/custom-admin.js' );
}
add_action( 'admin_enqueue_scripts', 'load_admin_ap_plugin_js_style' );
*/
//-----------------------------------------------------

// ----------------------------------------------------
// load up scripts for charts only for specific page - http://canvasjs.com/jquery-charts/
if(isset($_GET['page']) && $_GET['page'] == "alex-plugin-experiments" || $_GET['page'] == "alex-plugin-experiment-details") {
	add_action( 'admin_enqueue_scripts', 'load_admin_charts' );
}

function load_admin_charts() {
	wp_enqueue_style( 'apPluginStylesheet', AP_PLUGIN_URL_PATH . '/css/custom-admin.css' );
	wp_enqueue_script( 'apPluginScript', AP_PLUGIN_URL_PATH . '/js/canvasjs.min.js' );
}
// ----------------------------------------------------


// ----------------------------------------------------
// form data handling via admin-post.php hook.
add_action( 'admin_post_apsubmitnewexp', 'prefix_admin_apsubmitnewexp' );
function prefix_admin_apsubmitnewexp() {
    //print_r($_POST);
	ap_dbase_insert_new_experiment( $_POST );
	// redirect to an admin page after data processing.
    wp_redirect(admin_url('admin.php?page=alex-plugin-experiments'));
   //exit();
}

add_action( 'admin_post_apsubmitselectexperiment', 'prefix_admin_apsubmitselectexperiment' );
function prefix_admin_apsubmitselectexperiment() {
	// just redirect to a page.
    wp_redirect(admin_url('admin.php?page=alex-plugin-experiment-details&expid='.$_POST['experiments']));
}

add_action( 'admin_post_apsubmitsettings', 'prefix_admin_apsubmitsettings' );
function prefix_admin_apsubmitsettings() {
	if( $_POST['test'] ) {
		$value = "true";
	} else {
		$value = "false";
	}
	update_option('alex_plugin_settings', $value);
	ap_createTempMessage("success|Settings have been saved.|".$id);
	// redirect to an admin page after data processing.
    wp_redirect(admin_url('admin.php?page=alex-plugin-settings'));
}

add_action( 'admin_post_apsubmitupdatedvariation', 'prefix_admin_apsubmitupdatedvariation' );
function prefix_admin_apsubmitupdatedvariation() {
	ap_dbase_update_variation( $_POST );
    wp_redirect(admin_url('admin.php?page=alex-plugin-experiment-details&expid='.$_POST['expid']));
}

add_action( 'admin_post_apsubmitupdatedexperiment', 'prefix_admin_apsubmitupdatedexperiment' );
function prefix_admin_apsubmitupdatedexperiment() {
	ap_dbase_update_experiment( $_POST );
	if($_POST['paging'] == 0){
		$paging = "";
	}else{
		$paging = "&paging=".$_POST['paging'];
	}
    wp_redirect(admin_url('admin.php?page=alex-plugin-experiments'.$paging));
}
// ----------------------------------------------------

function cookie_handler() {
	if(empty($_COOKIE['wpms_experiment'])){
		setcookie("wpms_experiment", "myValue", time()+AP_COOKIE_EXPIRY, "/"); // 86400 for 1 day
		$experiments = ap_dbase_get_running_experiments();
		foreach($experiments as $experiment){
			$randomize = rand(0,1);
			if($randomize == 1) {
				$variation_id = ap_dbase_get_running_variation( $experiment->id );
			}else{
				$variation_id = null;
			}
			setcookie("wpms_experiment_".$experiment->id, $variation_id, time()+AP_COOKIE_EXPIRY, "/"); // 86400 for 1 day
		}
	}
}

// ----------------------------------------------------
// Shortcode implementation (display variation value by experiment id called)
// this triggers visit counts for either original or variation
// will depend on the cookie of experiment id to set a fixed variation id for a user per experiment.
// sample shortcode = [apPlugin expid="1"]
function ap_implement_shortcode( $atts ) {
    $args = shortcode_atts( array(
        'expid' => ''
    ), $atts );
	
	if($_COOKIE['wpms_experiment'] == null) {
		//functionads();
	}
	else
	{
		// check first if search engine bot, else proceed with variation and updating impression.
		if(false == ap_is_search_engine_bots())
		{
			if(true == ap_dbase_check_experiment_status( $args['expid'] )) // only update impression if experiment status is set to running.
			{	
				//$randomize_content = rand(0,1); //randomize loading of content (original vs. variations)
				//if($randomize_content == 1){
					
				$cookie_var = $_COOKIE['wpms_experiment_'.$args['expid']]; //cookie value will dictate the variation id of the current experiment
				if(!is_null($cookie_var)) {
					if(!($variation_content = ap_dbase_get_variation( $args['expid'], $cookie_var ))=== false)
					{
						ap_dbase_update_impression($args['expid'], $type = null);
						return $variation_content;
					}
				}else{
					ap_dbase_update_impression($args['expid'], $type = null);
				}
			}
		}
	}
}
// add_shortcode(shortcode_name, hook)
add_shortcode( 'apPlugin', 'ap_implement_shortcode' );
add_shortcode( 'applugin', 'ap_implement_shortcode' );
// -----------------------------------------------------