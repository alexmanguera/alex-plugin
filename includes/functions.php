<?php 

// ----------------
// delete a variation from under experiment details page.
// requires experiment id and variation id - GET method.
if($_GET['page'] == "alex-plugin-experiment-details" && $_GET['action'] == "delete-variation"){
	ap_trigger_delete_variation( $_GET['varid'], $_GET['expid'] );
}

function ap_trigger_delete_variation( $var_id, $exp_id ) {
	ap_delete_variation( $var_id );
	ap_createTempMessage("success|Variation has been deleted.|".$var_id);
	header('Location: admin.php?page=alex-plugin-experiment-details&expid='.$exp_id);
}
// ----------------

// ----------------
// delete an experiment along with its variation/s from experiments page.
// requires experiment id - GET method.
if($_GET['page'] == "alex-plugin-experiments" && $_GET['action'] == "delete-experiment"){
	ap_trigger_delete_experiment( $_GET['expid'] );
}

function ap_trigger_delete_experiment( $exp_id ) {
	ap_delete_experiment( $exp_id );
	ap_createTempMessage("success|Experiment has been deleted.|".$exp_id);
	header('Location: admin.php?page=alex-plugin-experiments&expid='.$exp_id);
}
// ----------------




/**
 * hook when plugin is activated.
 */
function activate() {
	$arr = array(
				"alex_plugin_version" => '0.1',
				"txt_def_distance_miles" => "A Lot of Miles",
				"txt_def_distance_kilometers" => "A Lot of Kilometers",
				"txt_def_home" => 'Your home or address',
				"txt_def_latitude" => 'Your Latitude',
				"txt_def_longitude" => 'Your Longitude',
				"txt_def_ip" => 'Your IP',
				"txt_def_city" => 'Your City',
				"txt_def_state_code" => 'Your State Code',
				"txt_def_country_code" => 'Your Country',
				"txt_def_country_name" => 'Your Country', 
				"chk_post_content" => "1",
				"chk_comments" => "1",
				"txt_filter_cls" => "",
				"rdo_units" => "miles",
				"rdo_case" => "off",
				"chk_default_options_db" => ""
				);
	update_option( AP_OPTION, $arr);
	ap_dbase_create_experiments();
	ap_dbase_create_variations();
	
	update_option( AP_OPTION_SETTINGS, "false");
}

/**
 * hook when plugin is deactivated.
 */
function deactivate() {
	//delete_option( AP_OPTION );
	//ap_dbase_drop_experiments();
}

/**
 * Create the Admin Menus.
 */
function ap_establish_menus() {
	add_menu_page('Alex Plugin', 'Alex Plugin', 'manage_options', 'alex-plugin-experiments', 'ap_render_alex_experiments', AP_PLUGIN_URL_PATH . '/assets/icon.png' );
	add_submenu_page( 'alex-plugin-experiments', 'Alex Plugin Experiments', 'Experiments', 'manage_options', 'alex-plugin-experiments', 'ap_render_alex_experiments' ); 
	add_submenu_page( 'alex-plugin-experiments', 'Experiment Details', 'Experiment Details', 'manage_options', 'alex-plugin-experiment-details', 'ap_render_alex_experiment_details' );
	add_submenu_page( 'alex-plugin-experiments', 'Alex Plugin Settings', 'Settings', 'manage_options', 'alex-plugin-settings', 'ap_render_alex_settings' );
	
	// add a page without creating a menu link (set 'null' as parent-slug)
	//add_submenu_page( 'null', 'Add New Experiment', 'Experiments', 'manage_options', 'alex-plugin-new-experiment', 'ap_render_alex_new_experiment' );
	
	// fix to remove duplicate submenu of main menu
	//remove_submenu_page('alex-plugin', 'alex-plugin');
}

/**
 * Render the view for "Experiments" page.
 */
function ap_render_alex_experiments() {
	// custom enqueue should have unique name
	wp_enqueue_script('jquery-ui-datepicker');
	wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
	wp_enqueue_script( 'apPluginScriptCustom', AP_PLUGIN_URL_PATH . '/js/custom-admin.js' );
	
		// show edit experiment page else show all experiments page
		if(!empty($_GET['action']) && $_GET['action'] == "edit" && !empty($_GET['expid'])) {
			include_once( WP_PLUGIN_DIR.  '/alex-plugin/views/alexEditExperiment.php' );
		}else{
			include_once( WP_PLUGIN_DIR.  '/alex-plugin/views/alexExperiment.php' );
		}
}

/**
 * Render the view for "Settings" page.
 */
function ap_render_alex_experiment_details() {
	include_once( WP_PLUGIN_DIR. '/alex-plugin/views/alexExperimentDetails.php' );
}


/**
 * Render the view for "Add New Experiments" page.
 */
function ap_render_alex_new_experiment() {
	include_once( WP_PLUGIN_DIR.  '/alex-plugin/views/alexNewExperiment.php' );
}

/**
 * Render the view for "Settings" page.
 */
function ap_render_alex_settings() {
	include_once( WP_PLUGIN_DIR. '/alex-plugin/views/alexSettings.php' );
}

//-----------------------------------------------------
/**
 * Create the experiments table when plugin is installed and activated.
 */
function ap_dbase_create_experiments() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$charset_collate = $wpdb->get_charset_collate();
	
	$sql = "CREATE TABLE IF NOT EXISTS {$table_name} (
					id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					name VARCHAR(250) NOT NULL DEFAULT '',
					description VARCHAR(500) NOT NULL DEFAULT '',
					status VARCHAR(25) NOT NULL DEFAULT '',
					start_date DATE NOT NULL DEFAULT '0000-00-00 00:00:00',
					end_date DATE NOT NULL DEFAULT '0000-00-00 00:00:00',
					goal VARCHAR(500) NOT NULL DEFAULT '', 
					goal_type VARCHAR(100) NOT NULL DEFAULT '',
					url VARCHAR(500) NOT NULL DEFAULT '',
					original_visits INT NOT NULL DEFAULT 0,
					original_convertions INT NOT NULL DEFAULT 0,
					date_created DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
					) $charset_collate;";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta( $sql );
}

/**
 * Create the variations table when plugin is installed and activated.
 */
function ap_dbase_create_variations() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	
	$sql = "CREATE TABLE " . $table_name . " (
					id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
					experiment_id INT NOT NULL,
					type VARCHAR(100) NOT NULL DEFAULT '',
					name VARCHAR(250) NOT NULL DEFAULT '',
					value TEXT NOT NULL DEFAULT '',
					class VARCHAR(500) NOT NULL DEFAULT '',
					visits INT NOT NULL DEFAULT 0,
					convertions INT NOT NULL DEFAULT 0,
					date_created DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
					);";

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
	dbDelta( $sql );
}


/**
 * Remove the DBASE when plugin is uninstalled.
 */
function ap_dbase_drop_experiments( $table ) {
	//$table = 'alex_plugin_experiments';
	global $wpdb;
	$table_name = $wpdb->prefix . $table;
	$sql = "DROP TABLE IF EXISTS {$table_name}";
	$wpdb->query( $sql ); // only $wpdb->query() works with DROP statement
}

/**
 * Insert to DBASE new experiment.
 */
function ap_dbase_insert_new_experiment( $arr_post ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	
	$today = date("Y-m-d");
	
	if($today > $arr_post['endDate']) {
		$status = 'complete';
	}elseif($today < $arr_post['startDate']) {
		$status = 'paused';
	}elseif($today >= $arr_post['startDate'] && $today <= $arr_post['endDate']) {
		$status = 'running';
	}
	
	// format -- $wpdb->insert( $table, $data, $format );
	$wpdb->insert( 
					$table_name, 
					array( 
						'name' => $arr_post['name'], 
						'description' => $arr_post['description'],
						'start_date' => $arr_post['startDate'],
						'end_date' => $arr_post['endDate'],
						'status' => $status,
						'goal' => $arr_post['goal'],
						'goal_type' => $arr_post['goalTrigger'],
						'url' => $arr_post['url'],
						'date_created' => date('Y-m-d H:i:s')
					)
				);
	$new_experiment_id = $wpdb->insert_id; // retrieve the id of the last insert.
		
	for($i = 0; $i <= count($arr_post['variationName']); $i++) {
		$name = $arr_post['variationName'][$i];
		$value = $arr_post['variation'][$i];
		$class = $arr_post['variationClass'][$i];
		
		ap_dbase_insert_new_variations( $new_experiment_id, $name, $value, $class );
	}
	
	ap_createTempMessage("success|New Experiment has been created|".$new_experiment_id);
}

/**
 * Insert to DBASE variations from new experiments.
 */
function ap_dbase_insert_new_variations( $new_experiment_id, $name, $value, $class ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	
	// format -- $wpdb->insert( $table, $data, $format );
	$wpdb->insert( 
					$table_name, 
					array( 
						'experiment_id' => $new_experiment_id,
						'type' => 'text',
						'name' => $name ,
						'value' => $value ,
						'class' => $class,
						'date_created' => date('Y-m-d H:i:s')
					)
				);
}

/**
 * get all experiments from DBASE (either paginated results).
 *
 * @return array
 */
function ap_dbase_get_experiments($offset = null, $limit = null) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	
	if(is_null($offset)) {
		$sql = "SELECT * FROM {$table_name} Order By id DESC";
	} else {
		$sql = "SELECT * FROM {$table_name} Order By id DESC LIMIT {$offset},{$limit}";
	}
	$results = $wpdb->get_results( $sql, OBJECT );
	return $results;
}

/**
 * get an experiment from DBASE by experiment_id.
 *
 * @params	$experiment_id	integer
 * @return array
 */
function ap_dbase_get_specific_experiment( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "SELECT * FROM {$table_name} WHERE id = '{$experiment_id}'";
	$results = $wpdb->get_results( $sql, OBJECT );
	return $results;
}

/**
 * check the status of an experiment if valid (running).
 *
 * @params	$experiment_id	integer
 * @return	boolean
 */
function ap_dbase_check_experiment_status( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "SELECT status FROM {$table_name} WHERE id = '{$experiment_id}'";
	$results = $wpdb->get_results( $sql, OBJECT );
	
	if($results)
	{
		foreach($results as $result){
			if($result->status == 'running') {
				return true;
			}else{
				return false;
			}
		}
	}
	else
	{
		return false;
	}
}

/**
 * get all experiments from DBASE for COOKIES implementation (only running experiments).
 *
 * @return array
 */
function ap_dbase_get_running_experiments() {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "SELECT * FROM {$table_name} WHERE status LIKE '%running%' Order By id DESC";
	$results = $wpdb->get_results( $sql, OBJECT );
	return $results;
}

/**
 * get all total variations per experiment.
 *
 * @return integer
 */
function ap_dbase_get_specific_total_variations( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "SELECT COUNT(*) AS total FROM {$table_name} WHERE experiment_id = {$experiment_id}";
	$results = $wpdb->get_results( $sql, OBJECT );
	foreach($results as $total)
	{
		$total_visits = $total->total;
	}	
	return $total_visits;
}

/**
 * get total visits by experiment id from experiments table.
 *
 * @param	integer	experiment_id
 * @return	integer
 */
 function ap_dbase_get_specific_total_visits_experiment( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "SELECT * FROM {$table_name} WHERE id = {$experiment_id}";
	$results = $wpdb->get_results( $sql, OBJECT );
	foreach($results as $totals)
	{
		$total_visits = $totals->original_visits;
	}	
	return $total_visits;
 }
 
/**
 * update experiment details.
 *
 * @param	string	experiment_id - experiment id.
 */
function ap_dbase_update_experiment( $arr_post ){
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	
	$today = date("Y-m-d");
	
	if($today > $arr_post['endDate2']) {
		$status = 'complete';
	}elseif($today < $arr_post['startDate2']) {
		$status = 'paused';
	}elseif($today >= $arr_post['startDate2'] && $today <= $arr_post['endDate2']) {
		$status = 'running';
	}
	
	$sql = "UPDATE {$table_name} SET 
					name = '{$arr_post['name']}',
					description = '{$arr_post['description']}',
					start_date='{$arr_post['startDate']}',
					end_date='{$arr_post['endDate']}',
					goal='{$arr_post['goal']}',
					goal_type='{$arr_post['goalTrigger']}',
					url='{$arr_post['url']}',
					status='{$status}'
			WHERE id = {$arr_post['expid']}";
	$wpdb->query( $sql );
}
 
/**
 * update variation details.
 *
 * @param	string	variation_id - variation id.
 */
function ap_dbase_update_variation( $arr_post ){
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "UPDATE {$table_name} SET name = '{$arr_post['variation_name']}', value='{$arr_post['variation_value']}' WHERE id = {$arr_post['varid']}";
	$wpdb->query( $sql );
}

/**
* delete a specific experiment and its variation/s.
*
* @param	integer	experiment_id
*/
function ap_delete_experiment( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$table_name2 = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "DELETE FROM {$table_name} WHERE id = '{$experiment_id}'";
	$sql2 = "DELETE FROM {$table_name2} WHERE experiment_id = '{$experiment_id}'";
	$wpdb->query($sql);
	$wpdb->query($sql2);
}
 
/**
* delete a specific variation.
*
* @param	integer	variation_id
*/
function ap_delete_variation( $variation_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "DELETE FROM {$table_name} WHERE id = '{$variation_id}'";
	$wpdb->query($sql);
}

/**
 * IMPRESSIONS - increment the original_visits/impressions for experiment.
 *
 * @param	string	id - experiment_id based on shortcode.
 */
function ap_dbase_update_impression($id, $type){
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "UPDATE {$table_name} SET original_visits = original_visits + 1 WHERE id = {$id}";
	$wpdb->query( $sql );
}

/**
 * get variations by experiment id.
 *
 * @param	integer	experiment_id
 * @return	array
 */
function ap_dbase_get_variations( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "SELECT * FROM {$table_name} WHERE experiment_id = {$experiment_id}";
	$results = $wpdb->get_results( $sql, OBJECT );

	return $results;
}

/**
 * get variations by experiment id for COOKIES implementation.
 *
 * @param	integer	experiment_id
 * @return	integer	variation_id
 */
function ap_dbase_get_running_variation( $experiment_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "SELECT * FROM {$table_name} WHERE experiment_id = {$experiment_id} ORDER BY RAND() LIMIT 1";
	$results = $wpdb->get_results( $sql, OBJECT );

	if($results){
		foreach($results as $result)
		{
			$variation_id = $result->id;
		}	
		return $variation_id;
	}
	else{
		return false;
	}
}

/**
 * IMPRESSIONS->VARIATIONS - get variation from specified experiment to display via shortcode.
 *
 * @param	string	experiment id - experiment_id based on shortcode.
 */
function ap_dbase_get_variation( $experiment_id, $variation_id){
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	
	if( $variation_id == null ) 
	{
		$sql = "SELECT id, value FROM {$table_name} WHERE experiment_id = {$experiment_id} ORDER BY RAND() LIMIT 1";
	}else{
		$sql = "SELECT id, value FROM {$table_name} WHERE experiment_id = {$experiment_id} AND id = {$variation_id}";
	}
	
	$results = $wpdb->get_results( $sql, OBJECT );
	
	if($results){
		foreach($results as $resval)
		{
			$value_content = $resval->value;
			// update the visitor count for this variation
			$sql = "UPDATE {$table_name} SET visits = visits + 1 WHERE id = {$resval->id}";
			$wpdb->query( $sql );
		}	
		return $value_content;
	}
	else
	{
		return false;
	}
}

/**
 * get total visits of a variation via variation id.
 *
 * @param	integer	variation_id
 * @return	integer
 */
function ap_dbase_get_specific_total_visits_variation( $variation_id ) {
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_variations';
	$sql = "SELECT * FROM {$table_name} WHERE id = {$variation_id}";
	$results = $wpdb->get_results( $sql, OBJECT );
	foreach($results as $totals)
	{
		$total_visits = $totals->visits;
	}	
	return $total_visits;
}

/**
 * update the experiment status (via transient)
 *
 */
function ap_dbase_update_experiment_status( $experiment_id, $status ){
	global $wpdb;
	$table_name = $wpdb->prefix . 'alex_plugin_experiments';
	$sql = "UPDATE {$table_name} SET status = '{$status}' WHERE id = '{$experiment_id}'";
	$wpdb->query( $sql );
}
//-----------------------------------------------------

// check experiment status ( set by transient [cache] )
function ap_check_experiment_status() {
		if(!get_transient('wpms_experiment_status'))
		{
			$experiments = ap_dbase_get_experiments($offset = null, $limit = null);
			foreach ($experiments as $experiment) {
				$startDate = $experiment->start_date;
				$endDate = $experiment->end_date;
				$today = date("Y-m-d");

				if($today > $endDate) {
					ap_dbase_update_experiment_status( $experiment->id, 'complete' );
				}elseif($today < $startDate) {
					ap_dbase_update_experiment_status( $experiment->id, 'paused' );
				}elseif($today >= $startDate && $today <= $endDate) {
					ap_dbase_update_experiment_status( $experiment->id, 'running' );
				}
			}
			// available constants ( MINUTE_IN_SECONDS, HOUR_IN_SECONDS, DAY_IN_SECONDS, WEEK_IN_SECONDS, YEAR_IN_SECONDS )
			set_transient( 'wpms_experiment_status', "updated", 60 );
		}
	}

//-----------------------------------------------------
/**
 * Create a flash message
 */
function ap_createTempMessage($message)
{
	// $message = [success/false]|[actual message]|[extras]
	$_SESSION['message'] = $message;
}

/**
 * Delete a flash message
 */
function ap_deleteTempMessage()
{
	$_SESSION['message'] = null;
}
//-----------------------------------------------------

/**
 * Determine if search engine bot/crawler.
 */
function ap_is_search_engine_bots() {
	$bots_arr = array(					
				'googlebot',
				'msnbot',
				'slurp',
				'ask jeeves',
				'crawl',
				'ia_archiver',
				'lycos'		
				);
				
	$bots = implode("|", $bots_arr);

	if(stripos($_SERVER['HTTP_USER_AGENT'], $bots) !== false){
		return true;
	}else{
		return false;
	}
}