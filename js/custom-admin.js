jQuery(document).ready(function() {
	
	//Event Dates - DatePicker
	jQuery( "#startDate" ).datepicker({
	  numberOfMonths: 2,
	  dateFormat: 'yy-mm-dd',
	  minDate: '0',
	  onClose: function( selectedDate ) {
		jQuery( "#endDate" ).datepicker( "option", "minDate", selectedDate );
	  }
	});
	jQuery( "#endDate" ).datepicker({
	  numberOfMonths: 2,
	  dateFormat: 'yy-mm-dd',
	  onClose: function( selectedDate ) {
		jQuery( "#startDate" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	
	// ------------------------------------------------
	//Event Dates - DatePicker
	jQuery( "#startDate2" ).datepicker({
	  numberOfMonths: 2,
	  dateFormat: 'yy-mm-dd',
	  minDate: '0',
	  onClose: function( selectedDate ) {
		jQuery( "#endDate2" ).datepicker( "option", "minDate", selectedDate );
	  }
	});
	jQuery( "#endDate2" ).datepicker({
	  numberOfMonths: 2,
	  dateFormat: 'yy-mm-dd',
	  onClose: function( selectedDate ) {
		jQuery( "#startDate2" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
		
});
/**********************************/
function addRow() {
	var myRow = '<tr>';
	
		myRow += '<td>';
		//myRow += '<label class="variation-label-name" for="variationName[]">Name</label>';
		myRow += '<input type="text" name="variationName[]" class="variationField" required>';
		myRow += '</td>';
		
		myRow += '<td>';
		//myRow += '<label class="variation-label" for="variation[]">Content</label>';
		myRow += '<input type="text" name="variation[]" class="variationField" required>';
		myRow += '</td>';
		
		myRow += '<td>';	
		//myRow += '<label class="class-label" for="class[]">Element Class</label>';
		myRow += '<input type="text" name="variationClass[]" class="variationField">';
		myRow += '</td>';
		
		myRow += '<td>';
		myRow += '<a href="#" onClick="jQuery(this).closest('+"'tr'"+').remove();" class="remove_field_button" style="text-decoration:none;">[-]</a>';
		myRow += '</td>';
		
		myRow += '</tr>';

	if(jQuery("#variants tr").length < 5)
	{
		jQuery("#variants tr:last").after(myRow);
	}
	else
	{
		alert('Maximum of 5 variants only.');
	}
}
/**********************************/
